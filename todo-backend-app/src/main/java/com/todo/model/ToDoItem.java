package com.todo.model;

import javax.persistence.*;

@Entity
@Table(name = "todo_list_items")
public class ToDoItem {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "item_description")
    private String description;

    @Column(name = "item_status")
    private String status;

    public ToDoItem(){

    }

    public ToDoItem(String description, String status){
        this.description = description;
        this.status = status;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.todo.model;

public enum TodoStatus {
    TODO("incomplete"),COMPLETED("completed");


    public final String label;
    private TodoStatus(String label){
        this.label = label;
    }
}

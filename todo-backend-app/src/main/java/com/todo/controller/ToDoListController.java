package com.todo.controller;

import com.todo.model.ToDoItem;
import com.todo.model.TodoStatus;
import com.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/todo")
@Component
public class ToDoListController {

    @Autowired
    private TodoService service;

    @GetMapping("/getActiveItems")
    public ResponseEntity<?> getActiveItems(){
        try{
            Optional<List<ToDoItem>> optionalItems = Optional.ofNullable(service.findItemsByStatus(TodoStatus.TODO.label));
            if(optionalItems.isPresent()){
                return new ResponseEntity<>(optionalItems.get(), HttpStatus.OK) ;
            }
            return new ResponseEntity<>("No item found with status: " + TodoStatus.TODO.label, HttpStatus.NOT_FOUND);
        } catch(Exception e){
            return errorResponse();
        }
    }

    @PostMapping("/addItem")
    public ResponseEntity<?> addItem(@RequestBody ToDoItem toDoItem ){
        try{
            return new ResponseEntity<>(service.addToDoItem(toDoItem), HttpStatus.CREATED);
        } catch(Exception e){
            e.printStackTrace();
            return errorResponse();
        }
    }

    private ResponseEntity<String> errorResponse(){
        return new ResponseEntity<>("Something went wrong :(", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

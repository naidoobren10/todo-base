package com.todo.service;

import com.todo.model.ToDoItem;

import java.util.List;

public interface TodoService {

    public List<ToDoItem> findItemsByStatus(String status);

    public ToDoItem addToDoItem(ToDoItem toDoItem);
}

package com.todo.service;

import com.todo.model.ToDoItem;
import com.todo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoRepository todoRepository;


    @Override
    public List<ToDoItem> findItemsByStatus(String status) {
        return todoRepository.findByStatus(status);
    }

    @Override
    public ToDoItem addToDoItem(ToDoItem toDoItem) {
        return todoRepository.save(toDoItem);
    }
}

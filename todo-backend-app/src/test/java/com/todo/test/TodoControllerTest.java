package com.todo.test;

import com.todo.controller.ToDoListController;
import com.todo.model.ToDoItem;
import com.todo.model.TodoStatus;
import com.todo.repository.TodoRepository;
import com.todo.service.TodoService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import java.util.Arrays;
import java.util.List;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;



@RunWith(SpringRunner.class)
@WebMvcTest(ToDoListController.class)
public class TodoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoService todoService;

    @MockBean
    private TodoRepository todoRepository;

    @Test
    public void getIncompleteItemsTest() throws Exception {
        ToDoItem toDoItem = new ToDoItem();
        toDoItem.setId(1l);
        toDoItem.setDescription("Todo Item 1");
        toDoItem.setStatus(TodoStatus.TODO.label);

        List<ToDoItem> toDoItems = Arrays.asList(toDoItem);
        when(todoService.findItemsByStatus(TodoStatus.TODO.label)).thenReturn(toDoItems);

        MvcResult result =  mockMvc.perform(get("/todo/getActiveItems")).andReturn();
        String expected = "[{\"id\":1,\"description\":\"Todo Item 1\",\"status\":\"incomplete\"}]";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }
}
